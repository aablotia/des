//package com.des.configurations;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//
//@Configuration
//@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
// @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("aablotia")
//                .password("{noop}123")
//                .roles("USER")
//                .accountLocked(false)
//                .accountExpired(false);
//
//
//        auth.inMemoryAuthentication()
//                .withUser("root")
//                .password("{noop}root")
//                .roles("ADMIN")
//                .accountLocked(false)
//                .accountExpired(false);
//
//    }
//}
