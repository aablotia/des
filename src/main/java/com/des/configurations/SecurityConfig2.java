package com.des.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig2  extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("aablotia")
                .password("{noop}123")
                .roles("USER")
                .accountLocked(false)
                .accountExpired(false);
        auth.inMemoryAuthentication()
                .withUser("root")
                .password("{noop}root")
                .roles("ADMIN")
                .accountLocked(false)
                .accountExpired(false);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http.authorizeRequests()

               .antMatchers("/","/login", "/page1", "/logout").permitAll()
               .antMatchers("/page2").permitAll()
               .antMatchers("/page3").hasRole("USER")
               .antMatchers("/page4").hasRole("/ADMIN")
                .and()
//       .formLogin()
//       .loginPage("/login")
//       .defaultSuccessUrl("/page1")

//               .and().exceptionHandling().accessDeniedPage("/login")
//               .and()
//            .logout()
//               .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//               .logoutSuccessUrl("/login")
//               .invalidateHttpSession(true)        // set invalidation state when logout
//               .deleteCookies("JSESSIONID")
//               .and()
//            .exceptionHandling()
//            .accessDeniedPage("/404");
                   ;}
}
