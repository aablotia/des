package com.des.controlers;


import com.des.beans.desUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AdminController {

    @RequestMapping("/")
    public String desHome(Model model) {
//        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println("Logged user's name is : " + user);
//        model.addAttribute("currentUser", user);
        return "/home";
    }


    @RequestMapping("/page1")
    public String page1(Model model) {
//        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println("Logged user's name is : " + user);
//        model.addAttribute("currentUser", user);

        return "/page1";
    }

    @RequestMapping("/login")
    public String page3(Model model) {
//        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println("Logged user's name is : " + user);
//        model.addAttribute("currentUser", user);
        return "/login";
    }


    @RequestMapping("/logout")
    public String logout(Model model) {
//        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println("Logged user's name is : " + user);
//        model.addAttribute("currentUser", user);
        return "page1";
    }
}
